# Resin Distributor

A simple printable device to help distribute resin in vacuum bagging. The base model is designed to work for 6mm (1/4 inch) tubing. 

<img src="img/ResinDistributor%20v11.png" width="600" />  


### Use
With a envelope vacuum bag use the base/protector to prevent sharp folds un der the distributor.


<img src="img/EnvelopeVacside.jpg" width="600" />  

<img src="img/EnvelopeBagFeedsideUnderVac.jpg" width="600" />  

<img src="img/EnvelopeBagVacSideWorking.jpg" width="600" />  

<img src="img/EnvelopeBagWorking.jpg" width="600" />  